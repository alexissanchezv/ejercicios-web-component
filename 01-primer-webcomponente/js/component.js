class SaludoBasicoElement extends HTMLElement {

    constructor() {
        super(); // Siempre mandar a llamar el constructor de la clase HTMLElement

        this.saludo = 'Hola, cómo estás';
        this.pintado = false;
    }

    // La fución "connectedCallback" se invoca cada vez que el elemento se conecte al DOM  (Cuando el componente se agrege al DOM de la pàgina)
    connectedCallback() {
        this.pintado = true;
        this.innerHTML = this.saludo;
    }

    // La función "disconnectedCallback" se invoca cuando desconectamos el elemento
    disconnectedCallback() {
        this.pintado = false;
    }

    // Obtenemos el valor de un atributo personalizado
    // Esta función se ejecuta antes de montar el elemento
    attributeChangedCallback(nombreAttr, viejoValor, nuevoValor) {
        if (nombreAttr === 'nombre') {
            this.saludo = `Hola ${nuevoValor}`;
        }
        if (this.pintado) {
            this.innerHTML = this.saludo;
        }
        console.log(`${nombreAttr} ha cambiado de ${viejoValor} a ${nuevoValor}`);
    }

    // Observamos los atributos que queremos
    static get observedAttributes() {
        return ['nombre'];
    }

}

// Registramos el elemento personalizado
customElements.define('saludo-basico', SaludoBasicoElement);