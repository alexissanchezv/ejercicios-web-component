class Counter extends HTMLElement {
    set _value(value) {
        this.setAttribute('value', value);
    }

    get _value() {
        return this.getAttribute('value');
    }

    set _max(max) {
        this.setAttribute('max', max);
    }

    get _max() {
        return this.getAttribute('max');
    }

    set _min(min) {
        this.setAttribute('min', min);
    }

    get _min() {
        return this.getAttribute('min');
    }

    set _step(step) {
        this.setAttribute('step', step);
    }

    get _step() {
        return this.getAttribute('step');
    }
    
    constructor () {
        super();

        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    connectedCallback() {
        try {
            if (isNaN(this._value))
                throw new Error('value');

            if (isNaN(this._step))
                throw new Error('step');

            if (isNaN(this._min))
                throw new Error('min');

            if (isNaN(this._max))
                throw new Error('max');
            
            this.render();

            this.inputCounter.setAttribute('value', this._value);
            this.btnIncrement.addEventListener('click', this.increment);
            this.btnDecrement.addEventListener('click', this.decrement);
        } catch(err) {
            console.error(`El valor del atributo "${err.message}" no es un valor numerico`);
        }
    }

    increment() {

        let nextValue = +this.inputCounter.getAttribute('value') + +this._step;
        this.inputCounter.setAttribute('value', nextValue);
    }

    decrement() {
        let nextValue = +this.inputCounter.getAttribute('value') - +this._step;
        this.inputCounter.setAttribute('value', nextValue);
    }

    render () {
        this.attachShadow({ mode: "open" });
        this.shadowRoot.innerHTML = `
            <div class="counterContainer">
                <button id="btn-increment">+</button>
                <input id="input-counter" readonly>
                <button id="btn-decrement">-</button>
            </div>
        `;

        this.btnIncrement = this.shadowRoot.querySelector('#btn-increment');
        this.inputCounter = this.shadowRoot.querySelector('#input-counter');
        this.btnDecrement = this.shadowRoot.querySelector('#btn-decrement');
    }
}

customElements.define('x-counter', Counter);