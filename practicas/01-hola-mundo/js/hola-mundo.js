class HolaMundoParagraph extends HTMLParagraphElement {
    static get observedAttributes() {
        return ['saludo'];
    }

    constructor() {
        super();
        this.saludo = "Saludo";
        this._onButtonClicked = this._onButtonClicked.bind(this);
    }

    _onButtonClicked() {
        alert(this.saludo);
    }

    connectedCallback() {
        this.innerText = this.saludo;
        this.addEventListener('click', this._onButtonClicked);
    }

    attributeChangedCallback(attr, oldVal, newVal) {
        if (attr === "saludo") {
            this.saludo = newVal;
        }
    }
}

customElements.define('hola-mundo', HolaMundoParagraph, { extends: 'p' });